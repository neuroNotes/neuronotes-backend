var express = require('express')
var path = require('path')
var app = express()
var cors = require('cors')
const port = 3000
var fs = require('fs')

var corsOptions = {
    origin: true,
    optionsSuccessStatus: 200,
    methods: ['POST'],
}
app.use(cors(corsOptions));
app.use(express.json());

var saveRoute = require('./routes/save')
var loadRoute = require('./routes/load')

app.use('/', express.static(path.join(__dirname, 'Neuro-Notetes')));
app.use('/save', saveRoute);
app.use('/load', loadRoute);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })
