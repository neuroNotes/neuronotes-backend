var express = require('express')
var router = express.Router()
var mysql = require('mysql')
var multer = require('multer')
const fs = require('fs');
var path = require('path');


var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'neuro'
})

connection.connect()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now(),req.method, req.originalUrl)
  next()
})

// define the about route
router.post('/node', function (req, res) {

  const index = req.body.index;

  connection.query(
    'DELETE C FROM `Nodes` N INNER JOIN `Connections` C ON C.ParentId = N.NodeId Or C.ChildId = N.Id WHERE ProjectId = ?;', 
    [index]
  );

  connection.query(
    'DELETE C FROM `Nodes` N INNER JOIN `DescConnections` C ON C.NodeId = N.NodeId WHERE ProjectId = ?;', 
    [index]
  );

  connection.query(
    'DELETE FROM `Nodes` WHERE ProjectId = ?;', 
    [index]
  );

  const nodes = req.body.nodes;

  nodes.forEach(node =>{
    connection.query('INSERT INTO `Nodes`(`NodeId`, `Name`, `X`, `Y`, `ProjectId`) VALUES (?,?,?,?,?)', [node.id, node.name, node.position.left, node.position.top, index], function (err, rows, fields) {
      if(err) {
        console.error(err);
        res.status(500).end();
      };
    })
  
    node.connectionsIds.forEach(conn => {
      connection.query('INSERT INTO `Connections`( `ParentId`, `ChildId`, `Text`) VALUES (?,?,?)', [node.id, conn.obj, conn.text | ''], (err,rows, fields) => {
        if(err) {
          console.error(err);
          res.status(500).end();
        };

      })
    }); 
    
    node.descriptionsIds.forEach(descCon => {
      connection.query('INSERT INTO `DescConnections`(`NodeId`, `DescId`) VALUES (?, ?)', [node.id, descCon], (err, rows, fields)=>{
        if(err) {
          console.error(err);
          res.status(500).end();
        };
      });
    });
  })
  res.status(201).send();
})


router.post('/desc', function (req, res) {
  const index = req.body.index;

  connection.query(
    'DELETE FROM `Description` WHERE ProjectId = ?;', 
    [index]
  );

  const desces = req.body.desces;
  desces.forEach(desc => {
    connection.query('INSERT INTO `Description`(`DescId`, `Text`, `isNested`, `X`, `Y`, `ImageId`, `ProjectId`) VALUES (?,?,?,?,?,?,?)', [desc.id, desc.text, desc.isNested, desc.position.left, desc.position.top, desc.imageId, index], (err, rows, fields)=>{
      if(err) {
        console.error(err);
        res.status(500).end();
      };
    });
  })
  res.status(201).send();
})

const StoragePath = 'files/';
router.post('/img', multer().single('uploadFile') ,async function (req, res) {
  try{
    // console.log(req.file);
    // console.log(req.body);
    if(!Number.isInteger(+req.body.id))
      return 0;

    const fileName = req.body.id+'.multi';

    if (!fs.existsSync(StoragePath)){
      fs.mkdirSync(StoragePath);
      fs.chown(StoragePath, 1000,1000, ()=> {});
    }
    fs.writeFile(StoragePath + fileName, req.file.buffer, (err) =>{
      if(err) {
        console.error(err);
        res.status(500).end();
      }else{
      connection.query('INSERT INTO `Images`(`ImageId`, `path`, `height`, `width`) VALUES (?,?,0,0) ON DUPLICATE KEY UPDATE path=?, height=0, width=0', [req.body.id, fileName, fileName], function (err, rows, fields){
        if(err) {
          console.error(err);
          res.status(500).end();
        }else{
          res.status(201).send();
        }
      })
      }
    });
  }
  catch (err){
    console.error(err);
    res.status(500).end();
  }
  finally{
    // res.status(201).send();
    req.socket.end();
  }

})

// router.post('/varibles', function (req, res){
//   connection.query('TRUNCATE TABLE LastId');
//   if(Number.isInteger(req.body.lastId)){
//     connection.query('INSERT INTO `LastId`(Id, `LastId`) VALUES (1, ?)', [req.body.lastId], (err, rows, fields)=>{
//       if(err) {
//         console.error(err);
//         res.status(500).end();
//       }else{
//         res.status(201).send();
//       }
//     });
//   }
//   res.status(201).send();
// });

router.post('/project', function(req, res){
  project = req.body;
  console.log(project)
  connection.query('INSERT INTO `Projects`(`ProjectId`, `Name`) VALUES (?,?)', [project.id, project.name], (err, rows, fields)=>{
    if(err) {
      console.error(err);
      res.status(500).end();
    }else{
      res.status(201).send();
    }
  });
});

module.exports = router