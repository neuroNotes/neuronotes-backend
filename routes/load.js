var express = require('express')
var router = express.Router()
var mysql = require('mysql')
var multer = require('multer')
const fs = require('fs');
var path = require('path');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'neuro'
})
  
connection.connect()
  
router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now(),req.method, req.originalUrl, res.statusCode);
    next()
});

router.get('/node/:index', function (req, res) {

    let index = req.params.index;
    console.log(index);
    let nodes = [];
    let nodesCon = [];
    let nodesDesc= [];
    connection.query("SELECT * FROM Nodes WHERE ProjectId = ?", [index] ,(err, rows) => {
        if (err) {
            console.error(err);
            res.status(500).end();
        }
        else {
            nodes = rows;
            connection.query("SELECT * FROM Connections", (err, rows)=>{
                if(err) {
                    console.error(err);
                    res.status(500).end();
                }
                else{
                    nodesCon = rows;
                    connection.query("SELECT * FROM DescConnections", (err, rows)=>{
                        if(err) {
                            console.error(err);
                            res.status(500).end();
                        }
                        else{
                            nodesDesc = rows;
                            nodes.forEach((node) => {
                                node.connections = [];
                                node.descriptions = [];
                                nodesCon.forEach(con => {
                                    if(con.ParentId === node.NodeId)
                                        node.connections.push({obj: con.ChildId, text: con.Text});
                                });
                                nodesDesc.forEach(descCon => {
                                    if(descCon.NodeId === node.NodeId)
                                        node.descriptions.push(descCon.DescId);
                                })
                            })
                            nodes = nodes.map(node => {
                                return {
                                    id: node.NodeId, 
                                    name: node.Name,
                                    position: {left: node.X, top: node.Y},
                                    connectionsIds: node.connections,
                                    descriptionsIds: node.descriptions
                                }
                            });

                            res.status(200).send(nodes);
                        }
                    });
                }
            });
        }
    });
});

router.get('/desc/:index', function(req, res) {
    let index = req.params.index;
    connection.query("SELECT * FROM Description WHERE ProjectId = ?", [index], (err, desces)=> {
        if(err) {
            console.error(err);
            res.status(500).end();
        }
        else{
            res.send(desces.map(desc => {
                return {
                    id: desc.DescId,
                    isNested: desc.isNested,
                    position: {top: desc.Y, left: desc.X},
                    text: desc.Text,
                    imageId: desc.ImageId
                }
            }))
        }}
        )
    
});

// router.get('/varibles',function (req, res){
//     let x;
//     connection.query('SELECT LastId from LastId where Id = 1 limit 1', [], (err, rows) => {
//         if(err){
//             console.error(err);
//             res.status(500).end();
//         }

//         if(rows.length > 0){
//             res.send({LastId: rows[0].LastId})
//         }
//         else
//             res.status(404).end();
        
//     })

// });

router.get('/image/:id',function (req, res){
    if(Number.isInteger(+req.params.id)){
        connection.query('SELECT * FROM Images WHERE ImageId = ? LIMIT 1', [req.params.id], (err, rows) => {
            if (err){
            console.error(err);
            res.status(500).end();
            }
            if(rows.length > 0)
                res.end(fs.readFileSync('files/'+rows[0].path));
            else
                res.status(404).end();
        })}
        else
            res.status(400).end();
});
router.get('/project', (req, res)=> {
    connection.query('SELECT * from Projects', (err, rows) => {
        if(err){
            console.error(err);
            res.status(500).end();
        }
        else{
            console.log(rows);
            res.status(201).send(rows);
        }
        
    })
});

module.exports = router
