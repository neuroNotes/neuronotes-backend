
-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Description`
--

CREATE TABLE `Description` (
  `Id` int(11) NOT NULL,
  `isNested` tinyint(1) NOT NULL,
  `X` int(11) NOT NULL,
  `Y` int(11) NOT NULL,
  `ImageId` int(11) NOT NULL,
  `Text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
