
-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Nodes`
--

CREATE TABLE `Nodes` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(50) NOT NULL,
  `X` int(11) NOT NULL,
  `Y` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
