
-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `DescConnections`
--

CREATE TABLE `DescConnections` (
  `Id` int(11) NOT NULL,
  `NodeId` int(11) NOT NULL,
  `DescId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
