
--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `Connections`
--
ALTER TABLE `Connections`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `DescConnections`
--
ALTER TABLE `DescConnections`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `Description`
--
ALTER TABLE `Description`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `Images`
--
ALTER TABLE `Images`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `LastId`
--
ALTER TABLE `LastId`
  ADD PRIMARY KEY (`Id`);

--
-- Indeksy dla tabeli `Nodes`
--
ALTER TABLE `Nodes`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `Connections`
--
ALTER TABLE `Connections`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `DescConnections`
--
ALTER TABLE `DescConnections`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `LastId`
--
ALTER TABLE `LastId`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
