
-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Connections`
--

CREATE TABLE `Connections` (
  `Id` int(10) UNSIGNED NOT NULL,
  `ParentId` int(11) NOT NULL,
  `ChildId` int(11) NOT NULL,
  `Text` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
